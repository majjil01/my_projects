import net.viralpatel.contact.form.Contact;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sun.tools.javac.resources.compiler;


public class Main {

	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring-servlet.xml");
		SessionFactory sf = (SessionFactory)ac.getBean("sessionFactory");
		Session s = sf.openSession();
		System.out.println(s);
		Transaction tx = s.beginTransaction();
		
		Contact c, c2, c3;
		c  = new Contact();
		//c.setId(123);
		c.setEmail("mln_sk@yahoo.com");
		c.setFirstname("LNRMLN");
		c.setLastname("MAJJIGA");
		c.setTelephone("9167072099");	
		System.out.println(c);
		s.save(c);
		System.out.println("COntact :: "+c);
		
		
		c2 = new Contact();
		c2.setEmail("mln_sk@yahoo.com");
		c2.setFirstname("Kumar");
		c2.setLastname("jammala");
		c2.setTelephone("1234567899");		
		s.saveOrUpdate(c);
		System.out.println("COntact :: "+c);
		
		
		c3 = new Contact();
		c3.setEmail("mln_sk@mail.com");
		c3.setFirstname("Lakshman");
		c3.setLastname("kuruba");
		c3.setTelephone("9052413024");		
		s.saveOrUpdate(c);
		System.out.println("COntact :: "+c);
		tx.commit();

	}

}
